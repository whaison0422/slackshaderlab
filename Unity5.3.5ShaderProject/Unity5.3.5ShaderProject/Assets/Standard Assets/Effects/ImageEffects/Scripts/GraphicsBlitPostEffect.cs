﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.ImageEffects
{
	[ExecuteInEditMode]
	[AddComponentMenu("Image Effects/GraphicsBlitPostEffect")]
	public class GraphicsBlitPostEffect : PostEffectsBase
	{
		public bool useBool=true;

		//---------------共通--------------
		public void setBool()
		{
			if (useBool == true) {
				useBool = false;
			} else {
				useBool = true;
			}
		}
		//--------------共通-----------------
		[Tooltip("GraphicsBlitPostEffect Material")]
		private Material material = null;

		private Camera cam;

		private Transform camtr;

		public Shader shader;
		// Called by camera to apply image effect
		public override bool CheckResources ()
		{
			//#if UNITY_IPHONE && !UNITY_EDITOR
			//if( UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5 )
			//{
			//Its a first generation iPhone
			//	useBool = false;
			//DestroyImmediate (this);
			//}
			//#endif
			CheckSupport (true);

			cam = GetComponent<Camera> ();
			camtr = cam.transform;

			shader= Shader.Find ("Hidden/GraphicsBlitPostEffect");//
			//fogMaterial.SetColor ("_FogColor", fogColor);
			material = CheckShaderAndCreateMaterial (shader, material);
			//material.SetColor ("_FogColor", fogColor);
			//material.SetFloat ("_Desaturate", Desaturate);
			if (!isSupported)
				ReportAutoDisable ();
			return isSupported;
		}

		void OnRenderImage (RenderTexture source, RenderTexture destination)
		{
			Graphics.Blit (source, destination, material);
		}

	}
}
