﻿Shader "Hidden/GraphicsBlitPostEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert//頂点プログラムへ頂点データの流し込み
			#pragma fragment frag

			//内蔵のシェーダー include ファイル
			//Unity は自身の 頂点シェーダーと Fragment シェーダーのプログラミング で、
			//定義済み変数やヘルパー関数を利用できる、いくつかのファイルを持たせることができます。
			//これは標準的な #include ディレクティブで実現できます、例えば、
			#include "UnityCG.cginc"//頂点プログラムへ頂点データの流し込み
			//UnityCG.cginc のデータ構造
			//struct appdata_base: 位置、法線、テクスチャ座標の、頂点シェーダー入力。

			uniform float _FLOAT=0.5;
			uniform float3 _FLOAT3=float3(0.1,1.0,0.1);

/*


			struct appdata_base {
			    float4 vertex : POSITION;
			    float3 normal : NORMAL;
			    float4 texcoord : TEXCOORD0;
			};
			//struct appdata_tan: 位置、法線、接線、ひとつのテクスチャ座標の、頂点シェーダー入力。
			struct appdata_tan {
			    float4 vertex : POSITION;
			    float4 tangent : TANGENT;
			    float3 normal : NORMAL;
			    float4 texcoord : TEXCOORD0;
			};
			//struct appdata_full: 位置、法線、接線、頂点カラー、ふたつのテクスチャ座標の、頂点シェーダー入力。
			struct appdata_full {
			    float4 vertex : POSITION;
			    float4 tangent : TANGENT;
			    float3 normal : NORMAL;
			    float4 texcoord : TEXCOORD0;
			    float4 texcoord1 : TEXCOORD1;
			    fixed4 color : COLOR;
			 };
			//struct appdata_img: 位置、ひとつのテクスチャ座標の、頂点シェーダー入力。
			struct appdata_img {
			    float4 vertex : POSITION;
			    half2 texcoord : TEXCOORD0;
			};
*/
			//http://docs.unity3d.com/ja/current/Manual/SL-BuiltinIncludes.html

			//これはidentityだとだいたい「単位行列」呼ばれる。値を1にする行列の基礎となる行列
			float4x4 identity(float4x4 dest){
				//Matrix4x4.identityだとこう
				//1  0  0  0
				//0  1  0  0
				//0  0  1  0
				//0  0  0  1
				//なんか変だな　まあなんでもいいから１にしたいののよね　きっと。
			     dest[0,0] = 0;
			     dest[1,0] = 0; 
			     dest[2,0] = 0; 
			     dest[3,0] = 1;
                 return dest;
			}

			//shaderLab内でビューカメラ行列を生成。
			float4x4 lookAt(float3 eye, float3 center,float3 up){//ビュー行列変換、目の位置、中視点、カメラUP
                  float4x4 dest;
				    float eyeX    = eye.x,    eyeY    = eye.y,    eyeZ    = eye.z,
				        upX     = up.x,     upY     = up.y,     upZ     = up.z,
				        centerX = center.x, centerY = center.y, centerZ = center.z;
				    if(eyeX == centerX && eyeY == centerY && eyeZ == centerZ){
                       //上に定義した関数。
				      return identity(dest);
				    }
				    float x0, x1, x2, y0, y1, y2, z0, z1, z2, l;
				    z0 = eyeX - center.x; z1 = eyeY - center.y; z2 = eyeZ - center.z;
				    l = 1 / sqrt(z0 * z0 + z1 * z1 + z2 * z2);
				    z0 *= l; z1 *= l; z2 *= l;
				    x0 = upY * z2 - upZ * z1;
				    x1 = upZ * z0 - upX * z2;
				    x2 = upX * z1 - upY * z0;
				    l = sqrt(x0 * x0 + x1 * x1 + x2 * x2);
				    if(!l){
				        x0 = 0; x1 = 0; x2 = 0;
				    } else {
				        l = 1 / l;
				        x0 *= l; x1 *= l; x2 *= l;
				    }
				    y0 = z1 * x2 - z2 * x1; y1 = z2 * x0 - z0 * x2; y2 = z0 * x1 - z1 * x0;
				    l = sqrt(y0 * y0 + y1 * y1 + y2 * y2);
				    if(!l){
				        y0 = 0; y1 = 0; y2 = 0;
				    } else {
				        l = 1 / l;
				        y0 *= l; y1 *= l; y2 *= l;
				    }
				    dest[0,1] = x0; dest[0,1] = y0; dest[0,2]  = z0; dest[0,3]  = 0;
				    dest[1,0] = x1; dest[1,1] = y1; dest[1,2]  = z1; dest[1,3]  = 0;
				    dest[2,0] = x2; dest[2,1] = y2; dest[2,2] = z2; dest[2,3] = 0;
				    dest[3,0] = -(x0 * eyeX + x1 * eyeY + x2 * eyeZ);
				    dest[3,1] = -(y0 * eyeX + y1 * eyeY + y2 * eyeZ);
				    dest[3,2] = -(z0 * eyeX + z1 * eyeY + z2 * eyeZ);
				    dest[3,3] = 1;
              return dest;
            }
            /*
			struct appdata
			{//頂点シェーダの受け取る型
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
            */
            struct appdata_whaison
            {//頂点シェーダの型
                float4 vertex : POSITION;
			    float4 tangent : TANGENT;
			    float3 normal : NORMAL;
			    float4 texcoord : TEXCOORD0;
			    float4 texcoord1 : TEXCOORD1;
			    float2 uv : TEXCOORD2;
			    fixed4 color : COLOR;//Vertex Color
			    //float4 color : COLOR;
			};
			struct v2f
			{//ピクセルシェーダの受け取る型
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;//Vertex Color
			};
			//Unity.cgincのappdata_baseを使う
			//v2f vert (appdata v)
			//v2f vert (appdata_base v)  
			//v2f vert (appdata_full v)  appdata_whaison
			v2f vert (appdata_whaison v)
			{
				v2f o;

                // 各行列を掛け合わせる順序を示す一例

                //[Unity] シェーダで使える定義済値
                //http://qiita.com/edo_m18/items/591925d7fc960d843afa
               
                //一旦モデルは後で
                

                //現在のビュー行列。 //カメラのパラメーターをシェーダ内部でパラメータ設定
                float3 eye   =float3(0.0, 0.0, 100.0); // カメラの位置 eye
                //[定義済み]_WorldSpaceCameraPos	float3	カメラのワールド空間位置 eye
                float3 center       = float3(0.0, 0.0, 0.0);  // カメラの注視点 center
                float3 up = float3(0.0, 1.0, 0.0);  // カメラの上方向 up
                //-----------------------------------------------
                float4x4  vMatrixFunc =lookAt( eye,  center, up);//shaderLab内でビューカメラ行列を生成。
               //---------------------------------------------------

                //float4x4  vMatrix= UNITY_MATRIX_V;//     View

                //現在の投影行列   
                float4x4 pMatrix= UNITY_MATRIX_P;//     Projection(投影方法)

                //この列オーダーなので 逆順番にするのが重要 Projection(投影方法)*viewPort(カメラ)*Model(頂点)
                // p*v*m  
                float4x4 vpMatrixFunc=mul(pMatrix, vMatrixFunc); // Projection に viewPort(カメラ) を掛ける////////////////////////

                 //現在のモデルマトリックス //もともとの頂点の位置を三次元空間のどこに頂点を置くのか を決める作業
                float4x4  mMatrix= _Object2World;//      Model


               float3 position1 = float3(-0.5,  0.5,  0.0);
               float3 position2 = float3(0.5,  0.5,  0.0);
               float3 position3 = float3(-0.5, -0.5,  0.0);
               float3 position4 = float3(0.5, -0.5,  0.0);

               //float4 vertexPosition=float4(-0.5,  0.5,  0.0);
        
               //http://docs.unity3d.com/ja/current/Manual/SL-VertexProgramInputs.html
               //頂点プログラムへ頂点データの流し込み
               // Cg/HLSL 頂点プログラム のために、メッシュ 頂点データは頂点シェーダー関数に入力データとして渡されます。各入力には、それを指定するセマンティクス が必要です。例えば、入力データ POSITION は頂点位置、NORMAL は頂点法線ベクトル、など。
               //頂点データの流し込みは、1つずつデータを渡す代わりに、しばしば 1つの構造体として宣言されます。
               //一般的に使用される頂点構造体は、UnityCG.cginc の include ファイル で定義され、たいていそれだけで十分です。
               //構造体には以下のものがあります。
               //UNITY_MATRIX_MVP=現在のモデルビュー行列×射影行列 （model*view*projection）

               	  //o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
               	 // o.pos = mul (vpMatrix, v.vertex);
		         // o.color.xyz = v.normal * 0.5 + 0.5;
		         // o.color.w = 1.0;

               //appdata_base: 位置、法線および 1 つのテクスチャ座標で構成されます。
               //appdata_tan: 位置、接線、法線および 1 つのテクスチャ座標で構成されます。
               //appdata_full: 位置、接線、法線、4 つのテクスチャ座標および色で構成されています。
               //例えば、このシェーダーは、その法線に基づいて、メッシュに色を付け、頂点プログラム入力として、appdata_base を使用します。

               //Unity でVBO (Vertex Buffer Object)は
               //https://docs.unity3d.com/ScriptReference/Mesh-vertices.html
               //csでMesh mesh = GetComponent<MeshFilter>().mesh;
               //Vector3[] vertices = mesh.vertices;
               /*
                    mMatrix[0,1] = 0.5; mMatrix[0,1] = 0.5; mMatrix[0,2]  = 0.5; mMatrix[0,3]  = 0.5;
				    mMatrix[1,0] = 0.5; mMatrix[1,1] = 0.5; mMatrix[1,2]  = 0.5; mMatrix[1,3]  = 0.5;
				    mMatrix[2,0] = 0.5; mMatrix[2,1] = 0.5; mMatrix[2,2] = 0.5; mMatrix[2,3] = 0.5;
				    mMatrix[3,0] = 0.5; mMatrix[3,1] = 0.5; mMatrix[3,2] = 0.5; mMatrix[3,3] = 0.5;
				*/
                //float4x4  
                //vMatrix= UNITY_MATRIX_V;//     View 使わない。

                //現在の投影行列   
                //float4x4 
                //pMatrix= UNITY_MATRIX_P;//     Projection(投影方法)

                //この列オーダーなので 逆順番にするのが重要 Projection(投影方法)*viewPort(カメラ)*Model(頂点)
                // p*v*m  
                float4x4 vpMatrix=mul(pMatrix, vMatrixFunc); // Projection に viewPort(カメラ) を掛ける

                mMatrix= _Object2World;//      Model
                //float4x4 
                //mvpMatrix=mul(vpMatrixFunc, v.vertex)); // さらに model(頂点) を掛ける////////////////////////////////////

                //o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);//UNITY_MATRIX_MVP///使わない//////////
                float4x4 UNITYvpMatrix=mul(UNITY_MATRIX_P, UNITY_MATRIX_V); // Projection に viewPort(カメラ) を掛ける
                float4x4 vFuncUNITYpMatrix=mul(UNITY_MATRIX_P, vMatrixFunc); // Projection に viewPort(カメラ) を掛ける
                float4 myvertex=float4(0.5,1.0,0.1,1.0);

                //モデルを動かし変えてみよう！
                /*
              int count=_Time.x*10;
            float nowTime = _Time;
            float radian = radius(count % 360);//gl3.TRI.rad[count % 360];
            // 平行移動（translate）-------------------------------------------
            float sin = sin(radian);
            float cos = cos(radian);
            float3 offset = (cos, sin, 0.0);
            //gl3.mat4.translate(mMatrix, offset, mMatrix);
          //  var offset = [1.0, 0.0, 0.0];
          //  gl3.mat4.translate(mMatrix, offset, mMatrix);

            // 回転（rotate）--------------------------------------------------
             float3 axis = float3(0.0, 1.0, 0.0);
             gl3.mat4.rotate(mMatrix, radian*10, axis, mMatrix);
             //gl3.mat4.rotate(mMatrix, uSin, axis, mMatrix);
             //var offset = [1.0, 0.0, 0.0];
             //gl3.mat4.translate(mMatrix, offset, mMatrix);
             */


            // 拡大縮小（scale）-----------------------------------------------
            // var uSin = Math.abs(Math.sin(radian));
            // var scale = [uSin, uSin, 1.0];
            // gl3.mat4.scale(mMatrix, scale, mMatrix);

           //o.vertexは float4 ですよーと。_Object2Worldは float4x4
               // o.vertex = mul(UNITYvpMatrix, (v.vertex*0.1)+myvertex*_Time.x);//UNITY_MATRIX_MVP///使わない//////////
                //o.vertex = mul(UNITYvpMatrix, v.vertex);
                float fl1=_Object2World[1,0];
                float fl2=_Object2World[1,1];
                float fl3=_Object2World[1,2];
                float fl4=_Object2World[1,3];

                float4 myFloat4=float4(fl1,fl2,fl3,fl4);
                //o.vertex = mul(UNITYvpMatrix,myFloat4);//_Object2World
                o.vertex = mul(UNITYvpMatrix,v.vertex);//v.vertex
                //o.vertex = UNITYvpMatrix;//_Object2World
                //o.vertex.x += 0.2 * v.normal.x * sin(o.vertex.y * 3.14 * 16);
                //o.vertex.z += 0.2 * v.normal.z * sin(o.vertex.y * 3.14 * 16);

                //float4x4 mvpMatrix=mul(vpMatrix, mMatrix); // さらに m を掛ける   
                //o.vertex = mul(mvpMatrix, v.vertex);

                float4x4 mvpMatrixFunc=mul(vpMatrixFunc, mMatrix);
                //o.vertex = mul(mvpMatrixFunc, v.vertex);

                //float4 vertex : POSITION;
                //float4	_Time
                //o.vertex = mul(mvpMatrix,float4(0.1,1.0,0.1,1.0));
                //o.vertex = mul(mvpMatrix,float4(_Time.x,_Time.y,_Time.z,1.0));
                //o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                float4 vertexPosition=float4(-0.5,  0.5,  0.0,1.0);
               	//o.vertex = mul (mvpMatrix, vertexPosition);
                //o.uv = v.uv;
                //return o;
                //unity.cging appdata_bace  float3 normal 
			    //unity.cging appdata_bace  float4 texcoord
				o.uv = float2(v.texcoord.x,v.texcoord.y);
                //fixed4 color : COLOR;
				//o.color.xyz = v.normal * 0.5 + 0.5;
				fixed v_color_xyz = v.normal * 0.5 + 0.5;
		        //o.color.w = 1.0;
		        //o.color=fixed4(v_color_xyz,v_color_xyz,v_color_xyz,1.0);
		        o.color=fixed4(1.0,1.0,1.0,1.0);
		        //o.color=v.color;
		        //o.color=float4(o.color.x,o.color.y,o.color.z,1.0);
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
			    fixed4 maintex = tex2D(_MainTex, i.uv);
				fixed4 col = fixed4(0.0,0.0,0.0,0.0);
				col = maintex;
				// 座標を正規化する//////////////////////////////////////sample_05.frag
				fixed2 p = i.uv;
				//col=fixed4(p.x,p.y,1.0+i.color.r,1.0);
				col=fixed4(p.x,p.y,1.0,1.0);

				// 座標を正規化し原点を中心に置く////////////////////////////sample_06.frag
				p = (p * 2.0 - 1.0);
                /// gl_FragColor = vec4(p, 0.0, 1.0);
                col = fixed4(p.x,p.y, 0.0, 1.0);
                ///各ピクセルの原点からの距離を計測し色として出力する/////////////////////sample_07.frag
                fixed leng=length(p);
                col = fixed4(leng,leng,leng, 1.0);
                // 強い光のような表現を行う一例//////////////////////////////sample_08.frag
                 fixed light =  (leng*0.20);

                 col = fixed4(light,light,light, 1.0);
                 //マイナス減算する
                  col=fixed4(maintex.r-light,maintex.g-light,maintex.b-light,1.0);
                   //col.color = i.color;
                   col=fixed4(col.x*i.color.x,col.y*i.color.y,col.z*i.color.z,col.w);//vertex color
                   col=fixed4(col.x+i.vertex.x,col.y+i.vertex.y,col.z+i.vertex.z,col.w);//vertex position
                   //col.color =fixed4(0.1,1.0,0.5,1.0);
				return col;
			}
			ENDCG
		}
	}
}
