﻿Shader "Motion/MotionVertex"
{
    SubShader {
        Tags { "RenderType" = "Opaque" }
        CGPROGRAM
        #pragma surface surf Lambert vertex:vert
        struct Input {
            float4 color : COLOR;
        };
        void vert (inout appdata_full v) {
            int count=_Time.x*10;
            float nowTime = _Time.y;
            //float radian = radius(count % 360);//gl3.TRI.rad[count % 360];
            // 平行移動（translate）-------------------------------------------
            //float sinf = _SinTime.z ;//sin(radian);//時間のサイン関数: (t/8、t/4、t/2、t)
            //float cosf = _CosTime.z;//cos(radian);//時間のコサイン関数: (t/8、t/4、t/2、t)

             float sinf = sin(_Time.y);//時間のサイン関数: (t/8、t/4、t/2、t)
            //float cosf = cos(radian);//時間のコサイン関数: (t/8、t/4、t/2、t)

            float3 offset = (sinf, sinf, 0.0);
            //v.vertex.x=v.vertex.x+offset.x*100;

            //v.vertex.x=sin(_Time.y*30);OK
            //v.vertex.y=v.vertex.y+offset.y;
            //v.vertex.z=v.vertex.z+offset.z;
            v.vertex.x += 0.2 * v.normal.x * sin(v.vertex.y * 3.14 * 16)*(sin(_Time.y*5));//時間 (t/20、t、t×2、t×3)
        }
        void surf (Input IN, inout SurfaceOutput o) {
            o.Albedo = half3(0.0, 0.0, sin(_Time.y*30));
        }
        ENDCG
    }
    Fallback "Diffuse"
}